[load_balancer_hosts]
${pub_ip_lb}

[application_hosts]
${pub_ip_app}

[all:vars]
ansible_ssh_private_key_file = ${key_path}
ansible_ssh_user = ${u_ssh}
